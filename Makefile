TAGGER=./hindi-pos-tagger/bin/tnt -v0 -H hindi-pos-tagger/models/hindi
LEMMATIZER=python3 ./hindi-pos-tagger/bin/lemmatiser.py hindi-pos-tagger/models/hindi.lemma
TAG2VERT=python3 ./bin/tag2vert.py
NORMALIZE=python3 ./bin/normalize_vert.py
POSMOD=python3 ./bin/modify_pos.py
ADDDUMMY=python3 ./bin/add_dummy_word.py
MERGE=python3 ./bin/merge.py
CONVERT_NULL=python3 ./bin/convert_NULL.py
CONVERT_FORMAT=python3 ./bin/convert_format.py
TOKENIZER=python3 ./bin/unitok.py -l hindi -n
RM=./Output/*.tmp.*

# Added Folders for Input and Output seperately 
INPUT=./Input/hindi.input.txt
OUTPUT=./Output/hindi.ouput

# Normalizer replaces some of the spellings with easy spellings on which the parser or MT systems work very well. 

$(OUTPUT):
	# uncomment below line if you require a normalizer
	cat $(INPUT) | $(TOKENIZER) | sed -e 's/।/./g' | sed -e 's/^\.$$/.\n<\/s>\n<s>/g' |  $(NORMALIZE)  > $@.tmp.words
	# uncomment below line if you do not require a normalizer
	# cat $< | $(TOKENIZER) |  sed -e 's/।/./g' | sed -e 's/^\.$$/.\n<\/s>\n<s>/g'  > $@.tmp.words
	$(TAGGER) $@.tmp.words | sed -e 's/\t\+/\t/g' | $(LEMMATIZER) | $(TAG2VERT) | $(POSMOD) | cut -f1,2,3 > $@.tmp.tag
	python3 bin/convert_format.py $@.tmp.tag $@.tmp.tag.conll
	java -jar bin/malt.jar -c test_complete -i $@.tmp.tag.conll -o $@.tmp.output -m parse
	python3 bin/convert_output.py $@.tmp.output $@
	rm  $(RM)
	echo "Output stored in $@"

